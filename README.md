<div align='center'>

# LeftWM

</div>

## 📦 Dependencies (Arch)

### ❗ Required

Official :

```shell
pacman -S xorg-server
```

AUR :

```shell
paru -S leftwm
```

### ➕ Optional

Official :

```shell
pacman -S \
  alacritty \
  choose \
  eva \
  feh \
  flameshot \
  nemo \
  noto-fonts-emoji \
  numlockx \
  picom \
  rofi \
  rofimoji \
  wireplumber \
  xclip \
  xorg-apps \
  xdotool \
  xss-lock \
```

AUR :

```shell
paru -S \
  betterlockscreen \
  mullvad-vpn-bin \
  rofi-greenclip \
  wired \
  zen-browser-avx2-bin \
```

## ⚙️ Configuration

To use _Ayu dark_ theme, use [this Eww configuration][1].

Apply the _Ayu dark_ theme :

```shell
ln -s ~/.config/leftwm/themes/ayu_dark ~/.config/leftwm/themes/current
```

Reboot and generate the Betterlockscreen background :

```shell
betterlockscreen -u ~/.config/leftwm/themes/ayu_dark/background.jpg
```

[1]: https://gitlab.com/FR6-3I_arch/configurations/eww
